require "./boc/math"
require "./boc/gfx/window"
require "./boc/gfx/context"
require "./boc/ui/panel"

module Boc
  include Math

  def self.run!
    window = Gfx::Window.new(800, 600, "Maggots")
    context = Gfx::Context.new()

    context.add_font("roboto", "resources/Roboto-Regular.ttf")
    panel = Ui::Panel.new()
      .dimensions=(Dimensions.new(width: 200, height: 150))
      .title=("Heyo")

    while !window.should_close()
      window.update do |width, height, pixel_ratio, delta_time|
        context.render width, height, pixel_ratio, do |context|
          panel.layout(Point.new(20, 20), Dimensions.new())
          panel.draw(context)
        end
      end
    end
  end

end

Boc.run!

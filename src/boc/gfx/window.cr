require "./lib/glfw"
require "./lib/gl"
require "./lib/nvg"

module Boc::Gfx
  class Window
    def initialize(width: Int32, height: Int32, title: String)
      initialize_glfw()
      @window = create_internal_window(width, height, title)
      @previous_time = GLFW.get_time()
    end

    def finalize
      GLFW.terminate()
    end

    def update
      GLFW.get_window_size(@window, out window_width, out window_height)
      GLFW.get_framebuffer_size(@window, out framebuffer_width, out framebuffer_height)
      pixel_ratio = window_width / NVG::Float.cast(framebuffer_width)

      time = GLFW.get_time()
      delta_time = @previous_time - time
      @previous_time = time

      GL.viewport(0, 0, framebuffer_width, framebuffer_height)
      GL.clear_color(0.2_f32, 0.2_f32, 0.2_f32, 1.0_f32)
      GL.clear(GL::DEPTH_BUFFER_BIT|GL::STENCIL_BUFFER_BIT|GL::COLOR_BUFFER_BIT)

      yield window_width, window_height, pixel_ratio, delta_time

      GLFW.swap_buffers(@window)
      GLFW.poll_events()
    end

    def should_close
      if GLFW::TRUE == GLFW.window_should_close(@window)
        true
      else
        false
      end
    end

    private def initialize_glfw
      if GLFW::TRUE != GLFW.init()
        raise "Failed to initialize GLFW"
      end

      GLFW.set_time(0.0)
      GLFW.set_error_callback ->(error_code, description) do
        print("GLFW error: [#{error_code}] #{description}\n")
      end
    end

    private def create_internal_window(width, height, title)
      window = GLFW.create_window(width, height, title, nil, nil)

      if nil == window
        GLFW.terminate()
        raise "Failed to create window"
      else
        GLFW.window_hint(GLFW::CONTEXT_VERSION_MAJOR, 3)
        GLFW.window_hint(GLFW::CONTEXT_VERSION_MINOR, 2)
        GLFW.window_hint(GLFW::OPENGL_FORWARD_COMPAT, GLFW::TRUE)
        GLFW.window_hint(GLFW::OPENGL_PROFILE, GLFW::OPENGL_CORE_PROFILE)
        GLFW.make_context_current(window)
      end

      window
    end

  end
end

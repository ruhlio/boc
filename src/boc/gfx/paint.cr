require "../math"
require "./lib/nvg"
require "./color"

module Boc::Gfx
  struct Paint
    include Math

    LARGE = 1e5_f32

    getter :nvg_paint

    def self.linear_gradient(
      start = Point.new,
      finish = Point.new,
      inner_color = Color,
      outer_color = Color
    )
      start_x = NVG::Float.cast(start.x)
      start_y = NVG::Float.cast(start.y)
      end_x = NVG::Float.cast(finish.x)
      end_y = NVG::Float.cast(finish.y)
      p = NVG::Paint.new()

      delta_x = end_x - start_x
      delta_y = end_y - start_y
      delta = Math.sqrt(delta_x * delta_x + delta_y * delta_y)
      if delta > 0.00001_f32
        delta_x /= delta
        delta_y /= delta
      else
        delta_x = 0.0_f32
        delta_y = 1.0_f32
      end

      p.xform[0] = delta_y
      p.xform[1] = -delta_x
      p.xform[2] = delta_x
      p.xform[3] = -delta_y
      p.xform[4] = start_x - delta_x * LARGE
      p.xform[5] = start_y - delta_y * LARGE

      p.extent[0] = LARGE
      p.extent[1] = LARGE + delta * 0.5_f32

      p.radius = 0.0_f32
      p.feather = Math.max(1.0_f32, delta)

      p.inner_color = inner_color.nvg_color
      p.outer_color = outer_color.nvg_color

      self.new(p)
    end

    def self.box_gradient(
      location = Point.new,
      dimensions = Dimensions.new,
      corner_radius = 0,
      feather = 0,
      inner_color = Color::NONE,
      outer_color = Color::NONE
    )
      x = NVG::Float.cast(location.x)
      y = NVG::Float.cast(location.y)
      width = NVG::Float.cast(dimensions.width)
      height = NVG::Float.cast(dimensions.height)
      corner_radius = NVG::Float.cast(corner_radius)
      feather = NVG::Float.cast(feather)
      p = NVG::Paint.new()

      p.xform[0] = 1.0_f32
      p.xform[1] = 0.0_f32
      p.xform[2] = 0.0_f32
      p.xform[3] = 1.0_f32
      p.xform[4] = x + width * 0.5
      p.xform[5] = y + height * 0.5

      p.extent[0] = width * 0.5
      p.extent[1] = height * 0.5

      p.radius = corner_radius
      p.feather = feather

      p.inner_color = inner_color.nvg_color
      p.outer_color = outer_color.nvg_color

      self.new(p)
    end

    def initialize(nvg_paint: NVG::Paint)
      @nvg_paint = nvg_paint
    end

    def to_unsafe
      pointerof(@nvg_paint)
    end

  end
end

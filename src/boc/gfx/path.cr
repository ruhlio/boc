require "../math"
require "./lib/nvg"

module Boc::Gfx
  include Math

  enum Winding
    CCW = 1
    CW = 2
  end

  enum Solidity
    Solid = 1
    Hole = 2
  end

  class Path

    def initialize(@context: NVG::Context)
    end

    def winding=(winding: Solidity|Winding)
      NVG.path_winding(@context, winding.value)
    end

    def stroke_color=(color: Color)
      NVG.stroke_color(@context, color)
    end

    def stroke_paint=(paint: Paint)
      NVG.stroke_paint(@context, paint)
    end

    def fill_color=(color: Color)
      NVG.fill_color(@context, color)
    end

    def fill_paint=(paint: Paint)
      NVG.fill_paint(@context, paint)
    end

    def move_to(point: Point)
      NVG.move_to(@context, NVG::Float.cast(point.x), NVG::Float.cast(point.y))
    end

    def line_to(point: Point)
      NVG.line_to(@context, NVG::Float.cast(point.x), NVG::Float.cast(point.y))
    end

    def rect(location: Point, dimensions: Dimensions)
      NVG.rect(@context, NVG::Float.cast(location.x), NVG::Float.cast(location.y), NVG::Float.cast(dimensions.width), NVG::Float.cast(dimensions.height))
    end

    def rounded_rect(location: Point, dimensions: Point, corner_radius: Number)
      NVG.rounded_rect(@context, NVG::Float.cast(location.x), NVG::Float.cast(location.y), NVG::Float.cast(dimensions.width), NVG::Float.cast(dimensions.height), NVG::Float.cast(corner_radius))
    end

    def stroke
      NVG.stroke(@context)
    end

    def fill
      NVG.fill(@context)
    end

  end
end

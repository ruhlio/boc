require "../math"
require "./lib/nvg"

module Boc::Gfx
  include Math

  @[Flags]
  enum Alignment
    Left = 1 << 0
    Center = 1 << 1
    Right = 1 << 2

    Top = 1 << 3
    Middle = 1 << 4
    Bottom = 1 << 5
    Baseline = 1 << 6
  end

  struct FontId
    def initialize(@value: Int32)
    end

    def is_valid?
      return -1 != @value
    end

    def to_unsafe
      @value
    end

  end

  class Text

    property :location, :content

    def initialize(@context: NVG::Context)
      @location = Point.new
      @content = ""
    end

    def alignment=(alignment: Alignment)
      NVG.text_align(@context, alignment.value)
    end

    def font_size=(size: Number)
      NVG.font_size(@context, NVG::Float.cast(size))
    end

    def font_face=(font: FontId|String)
      font_id = if font.is_a? FontId
        font
      else
        font_id = FontId.new(NVG.find_font(@context, font))
        raise "Invalid font \"#{font}\"" if !font_id.is_valid?
        font_id
      end
      
      NVG.font_face_id(@context, font_id)
    end

    def font_blur=(amount: Number)
      NVG.font_blur(@context, NVG::Float.cast(amount))
    end

    def stroke_color=(color: Color)
      NVG.stroke_color(@context, color)
    end

    def stroke_paint=(paint: Paint)
      NVG.stroke_paint(@context, paint)
    end

    def fill_color=(color: Color)
      NVG.fill_color(@context, color)
    end

    def fill_paint=(paint: Paint)
      NVG.fill_paint(@context, paint)
    end

  end
end

require "./lib/nvg"

module Boc::Gfx
  struct Color
    INT_TO_FLOAT = 255.0_f32

    getter :nvg_color

    def initialize(red, green, blue, alpha)
      @nvg_color = NVG::Color.new
      @nvg_color.red = red / INT_TO_FLOAT
      @nvg_color.green = green / INT_TO_FLOAT
      @nvg_color.blue = blue / INT_TO_FLOAT
      @nvg_color.alpha = alpha / INT_TO_FLOAT
    end

    def to_unsafe
      pointerof(@nvg_color)
    end

    NONE = Color.new(0, 0, 0, 0)
    BLACK = Color.new(0, 0, 0, 255)
    WHITE = Color.new(255, 255, 255, 255)

    GREY_50 = Color.new(250, 250, 250, 255)
    GREY_100 = Color.new(245, 245, 245, 255)
    GREY_200 = Color.new(238, 238, 238, 255)
    GREY_300 = Color.new(224, 224, 224, 255)
    GREY_400 = Color.new(189, 189, 189, 255)
    GREY_500 = Color.new(158, 158, 158, 255)
    GREY_600 = Color.new(117, 117, 117, 255)
    GREY_700 = Color.new(97, 97, 97, 255)
    GREY_800 = Color.new(66, 66, 66, 255)
    GREY_900 = Color.new(33, 33, 33, 255)

  end
end

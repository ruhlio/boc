@[Link("glfw3 --static")]
lib GLFW
  alias Boolean = Int32
  alias CString = UInt8*;

  type Window = Void*;
  type Monitor = Void*;
  type ErrorCallback = (Int32, CString) -> Void

  TRUE = 1
  FALSE = 0

  CLIENT_API                = 0x00022001
  CONTEXT_VERSION_MAJOR     = 0x00022002
  CONTEXT_VERSION_MINOR     = 0x00022003
  CONTEXT_REVISION          = 0x00022004
  CONTEXT_ROBUSTNESS        = 0x00022005
  OPENGL_FORWARD_COMPAT     = 0x00022006
  OPENGL_DEBUG_CONTEXT      = 0x00022007
  OPENGL_PROFILE            = 0x00022008
  CONTEXT_RELEASE_BEHAVIOR  = 0x00022009

  OPENGL_ANY_PROFILE      = 0
  OPENGL_CORE_PROFILE     = 0x00032001
  OPENGL_COMPAT_PROFILE   = 0x00032002

  fun set_error_callback = glfwSetErrorCallback(callback: ErrorCallback): ErrorCallback

  fun init = glfwInit(): Boolean
  fun terminate = glfwTerminate(): Void

  fun create_window = glfwCreateWindow(width: Int32, height: Int32, title: CString, monitor: Monitor, share: Window): Window
  fun window_hint = glfwWindowHint(target: Int32, hint: Int32): Void
  fun make_context_current = glfwMakeContextCurrent(window: Window): Void
  fun window_should_close = glfwWindowShouldClose(window: Window): Boolean
  fun set_window_should_close = glfwSetWindowShouldClose(window: Window, should_close: Boolean): Void
  fun get_window_size = glfwGetWindowSize(window: Window, width: Int32*, height: Int32*)
  fun get_framebuffer_size = glfwGetFramebufferSize(window: Window, width: Int32*, height: Int32*)

  fun swap_buffers = glfwSwapBuffers(window: Window): Void
  fun poll_events = glfwPollEvents(): Void

  fun get_time = glfwGetTime(): Float64
  fun set_time = glfwSetTime(time: Float64): Void

end

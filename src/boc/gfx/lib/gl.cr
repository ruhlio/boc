@[Link(framework: "OpenGL")] ifdef darwin
@[Link("GL")] ifdef linux
lib GL
  DEPTH_BUFFER_BIT = 0x00000100_u32
  STENCIL_BUFFER_BIT = 0x00000400_u32
  COLOR_BUFFER_BIT = 0x00004000_u32

  fun get_error = glGetError(): Int32

  fun viewport = glViewport(x: Int32, y: Int32, width: Int32, height: Int32): Void
  fun clear_color = glClearColor(red: Float32, green: Float32, blue: Float32, alpha: Float32): Void
  fun clear = glClear(mask: UInt32): Void
end

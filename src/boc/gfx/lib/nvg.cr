@[Link(ldflags: "-Llib -lnanovg")]
lib NVG
  alias Float = Float32
  alias CString = UInt8*;
  type Context = Void*

  ANTIALIAS       = 1 << 0
  STENCIL_STROKES = 1 << 1
  DEBUG           = 1 << 2

  struct Color
    red, green, blue, alpha : Float32
  end

  struct Paint
    xform: StaticArray(Float, 6)
    extent: StaticArray(Float, 2)
    radius: Float
    feather: Float
    inner_color: Color
    outer_color: Color
    image: Int32
  end

  enum LineCap
    Butt
    Round
    Square
    Bevel
    Miter
  end

  fun create_gl3 = nvgCreateGL3(flags: Int32): Context
  fun delete_gl3 = nvgDeleteGL3(context: Context): Void

  fun begin_frame = nvgBeginFrame(context: Context, window_width: Int32, window_height: Int32, pixel_ratio: Float): Void
  fun cancel_frame = nvgCancelFrame(context: Context): Void
  fun end_frame = nvgEndFrame(context: Context): Void

  fun save = nvgSave(context: Context)
  fun restore = nvgRestore(context: Context)
  fun reset = nvgReset(context: Context)

  fun begin_path = nvgBeginPath(context: Context)
  fun close_path = nvgClosePath(context: Context)
  fun path_winding = nvgPathWinding(context: Context, direction: Int32)
  fun move_to = nvgMoveTo(context: Context, x: Float, y: Float)
  fun line_to = nvgLineTo(context: Context, x: Float, y: Float)
  #fun bezier_to
  #fun quad_to
  #fun arc_to
  #fun arc
  fun rect = nvgRect(context: Context, x: Float, y: Float, width: Float, height: Float)
  fun rounded_rect = nvgRoundedRect(context: Context, x: Float, y: Float, width: Float, height: Float, radius: Float)
  # fun ellipse
  #fun circle
  fun stroke = nvgStroke(context: Context)
  fun fill = nvgFill(context: Context)

  fun stroke_color = nvgStrokeColor(context: Context, color: Color*)
  fun stroke_paint = nvgStrokePaint(context: Context, paint: Paint*)
  fun fill_color = nvgFillColor(context: Context, color: Color*)
  fun fill_paint = nvgFillPaint(context: Context, paint: Paint*)

  fun font_size = nvgFontSize(context: Context, size: Float)
  fun font_blur = nvgFontBlur(context: Context, amount: Float)
  fun create_font = nvgCreateFont(context: Context, name: CString, path: CString): Int32
  fun find_font = nvgFindFont(context: Context, name: CString): Int32
  fun font_face_id = nvgFontFaceId(context: Context, id: Int32)
  fun text_align = nvgTextAlign(context: Context, alignment: Int32)
  fun text = nvgText(context: Context, x: Float, y: Float, context: CString, end: UInt8*)

end

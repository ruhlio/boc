require "./lib/nvg"
require "./path"
require "./text"

module Boc::Gfx
  class Context

    def initialize
      @context = NVG.create_gl3(NVG::ANTIALIAS|NVG::STENCIL_STROKES|NVG::DEBUG)
      raise "Couldn't create Nanovg context" if nil == @context
    end

    def finalize
      NVG.delete_gl3(@context)
    end

    def render(width: Int32, height: Int32, pixel_ratio: NVG::Float)
      NVG.begin_frame(@context, width, height, pixel_ratio)
      yield self
      NVG.end_frame(@context)
    end

    def to_unsafe
      @context
    end

    # State handling

    def save
      NVG.save(@context)
    end

    def restore
      NVG.restore(@context)
    end

    def reset
      NVG.reset(@context)
    end

    # Drawing

    def draw_path
      NVG.begin_path(@context)
      yield Path.new(@context)
    end

    def draw_text
      text = Text.new(@context)
      yield text
      NVG.text(@context, NVG::Float.cast(text.location.x), NVG::Float.cast(text.location.y), text.content, nil)
    end

    # Fonts

    def add_font(name: String, path: String)
      font_id = FontId.new(NVG.create_font(@context, name, path))
      if font_id.is_valid?
        font_id
      else
        raise "Couldn't add font \"#{name}\" from path \"#{path}\""
      end
    end

  end
end

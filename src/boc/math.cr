module Boc::Math
  struct Spacing
    getter :left, :right, :top, :bottom

    def initialize(@left = 0, @top = 0, @right = 0, @bottom = 0)
    end

    def initialize(left_right: Number, top_bottom: Number)
      @left = @right = left_right
      @top = @bottom = top_bottom
    end

    def initialize(all: Number)
      @left = @top = @right = @bottom = all
    end

    def vertical
      @top + @bottom
    end

    def horizontal
      @left + @right
    end

  end

  struct Point
    getter :x, :y

    def initialize(@x = 0, @y = 0)
    end

    def +(point: Point)
      Point.new(@x + point.x, @y + point.y)
    end

    def -(point: Point)
      Point.new(@x - point.x, @y - point.y)
    end

    def +(spacing: Spacing)
      Point.new(@x + spacing.left, @y + spacing.top)
    end

    def -(spacing: Spacing)
      Point.new(@x - spacing.left, @y - spacing.top)
    end

  end

  struct Dimensions
    getter :width, :height

    def initialize(@width = 0, @height = 0)
    end

    def +(spacing: Spacing)
      Dimensions.new(@width + spacing.horizontal, @height + spacing.vertical)
    end

    def -(spacing: Spacing)
      Dimensions.new(@width - spacing.horizontal, @height - spacing.vertical)
    end

  end
end

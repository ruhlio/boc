require "../math"
require "../gfx/color"
require "../gfx/paint"
require "./widget"
require "./layout"

module Boc::Ui
  class Panel < Widget
    include Math
    include Gfx

    fluid_property :title, :layout

    def initialize
      super
      @location = Point.new
      @title = ""
      @layout = BlankLayout.new
    end

    def layout(location, area)
      @location = location
      #todo: represent that a panel defines area or take area if @dimensions is undefined?
      @layout.layout(location - @padding, area - @padding)
    end

    def draw(context)
      context.save()
      draw_background(context)
      draw_shadow(context)
      draw_header(context)
      context.restore()
    end

    private def draw_background(context)
      context.draw_path do |path|
        path.rect(@location, @dimensions)
        path.fill_color = Color.new(28, 30, 34, 192)
        path.fill()
      end
    end

    private def draw_shadow(context)
      paint = Paint.box_gradient(@location + Point.new(0, 2), @dimensions, 0, 10,
        Color.new(0, 0, 0, 128), Color.new(0, 0, 0, 0))
      context.draw_path do |path|
        path.rect(@location - Point.new(10, 10), @dimensions + Spacing.new(left: 20, top: 20))
        path.rect(@location, @dimensions)
        path.winding = Solidity::Hole
        path.fill_paint = paint
        path.fill()
      end
    end

    def draw_header(context)
      context.draw_path do |path|
        path.move_to(@location + Point.new(0.5, 30.5))
        path.line_to(@location + Point.new(@dimensions.width - 0.5, 30.5))
        path.stroke_color = Color.new(0, 0, 0, 32)
        path.stroke()
      end

      context.draw_text do |text|
        text.location = @location + Point.new(@dimensions.width / 2, 17)
        text.content = @title
        text.font_size = 18
        text.font_face = "roboto"
        text.font_blur = 0
        text.fill_color = Color.new(220, 220, 220, 160)
      end

    end

  end
end

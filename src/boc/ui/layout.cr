require "../math"
require "./widget"

module Boc::Ui
  abstract class Layout < Widget

    def initialize
      super
      @children = [] of Widget
    end

    def +(child: Widget)
      @children.push(child)
      self
    end

    def draw(context)
      @children.each do |child|
        child.draw(context)
      end
    end

  end

  class BlankLayout < Layout

    def layout(location, area)
    end

  end

  class HorizontalStack < Layout

    def layout(location, area)
      child_area = Dimensions.new(area.width, area.height / @children.count) - @padding
      @children.each do |child|
        child_location = location + padding
        child.layout(child_location, child_area)
      end
    end

  end
end

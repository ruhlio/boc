require "../math"
require "../gfx/context"

module Boc::Ui
  abstract class Widget
    include Math

    macro fluid_property(*names)
      getter {{*names}}

      {% for name in names %}
        {% if name.is_a?(DeclareVar) %}
          def {{name.var.id}}=(@{{name.var.id}} : {{name.type}})
            self
          end
        {% else %}
          def {{name.id}}=(@{{name.id}})
            self
          end
        {% end %}
      {% end %}
    end

    fluid_property :dimensions, :padding

    def initialize
      @dimensions = Dimensions.new
      @padding = Spacing.new
    end

    abstract def layout(location: Point, area: Dimensions)

    abstract def draw(context: Gfx::Context)

  end
end

module Boc
  class Diff(T)
    getter changes

    def initialize(source: Array(T), target: Array(T))
      lengths = compute_lcs_lengths(source, target)
      @changes = gather_changes(lengths, source, target, source.length - 1, target.length - 1)
    end

    private def compute_lcs_lengths(source, target)
      lengths = Hash(Array(Int32), Int32).new(0)

      source.each_with_index do |source_chunk, source_index|
        target.each_with_index do |target_chunk, target_index|
          if source_chunk == target_chunk
            lengths[[source_index, target_index]] = lengths[[source_index - 1, target_index - 1]] + 1
          else
            lengths[[source_index, target_index]] = Math.max(
              lengths[[source_index - 1, target_index]],
              lengths[[source_index, target_index - 1]]
            )
          end
        end
      end

      lengths
    end

    private def gather_changes(lengths, source, target, source_index, target_index)
      prev_source_length = lengths[[source_index - 1, target_index]]
      prev_target_length = lengths[[source_index, target_index - 1]]

      if source_index >= 0 && target_index >= 0 && source[source_index] == target[target_index]
        changes = gather_changes(lengths, source, target, source_index - 1, target_index - 1)
        changes.push Change.new(ChangeType::Same, source[source_index])
        changes
      elsif target_index >= 0 && (-1 == source_index || prev_target_length >= prev_source_length)
        changes = gather_changes(lengths, source, target, source_index, target_index - 1)
        changes.push Change.new(ChangeType::Insert, target[target_index])
        changes
      elsif source_index >= 0 && (-1 == target_index || prev_source_length >= prev_target_length)
        changes = gather_changes(lengths, source, target, source_index - 1, target_index)
        changes.push Change.new(ChangeType::Delete, source[source_index])
        changes
      else
        [] of Change
      end
    end
  end

  enum ChangeType
    Same
    Insert
    Delete
  end

  struct Change
    getter type, value

    def initialize(@type: ChangeType, @value: String)
    end
  end

end

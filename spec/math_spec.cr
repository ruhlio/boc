require "../src/boc/math"

include Boc::Math

describe Boc::Math do
  describe Spacing do
    it "calculates total vertical space" do
      Spacing.new(15).vertical.should eq 30
    end

    it "calculates total horizontal space" do
      Spacing.new(15).horizontal.should eq 30
    end

  end

  describe Point do
    it "can have another point added to it" do
      (Point.new(5, 5) + Point.new(10, 10)).should eq Point.new(15, 15)
    end

    it "can have another point subtracted from it" do
      (Point.new(30, 25) - Point.new(10, 5)).should eq Point.new(20, 20)
    end

    it "can have spacing added to it" do
      (Point.new(10, 15) + Spacing.new(5, 10)).should eq Point.new(15, 25)
    end

    it "can have spacing subtracted from it" do
      (Point.new(10, 15) - Spacing.new(5, 10)).should eq Point.new(5, 5)
    end
  end

  describe Dimensions do
    it "can have spacing added to it" do
      (Dimensions.new(10, 15) + Spacing.new(5, 10)).should eq Dimensions.new(20, 35)
    end

    it "can have spacing subtracted from it" do
      (Dimensions.new(10, 25) - Spacing.new(5, 10)).should eq Dimensions.new(0, 5)
    end
  end
end

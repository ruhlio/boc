require "spec"
require "../src/boc/diff"

include Boc

describe Boc::Diff do
  describe "when looking at the changes" do
    it "finds all of them" do
      source = ["first", "second", "third", "forth", "fifth", "sixth"]
      target = ["second", "third", "sixth", "seventh"]
      diff = Diff.new(source, target)
      diff.changes.should eq([
        Change.new(ChangeType::Delete, "first"),
        Change.new(ChangeType::Same, "second"),
        Change.new(ChangeType::Same, "third"),
        Change.new(ChangeType::Delete, "forth"),
        Change.new(ChangeType::Delete, "fifth"),
        Change.new(ChangeType::Same, "sixth"),
        Change.new(ChangeType::Insert, "seventh"),
      ])
    end
  end 
end
